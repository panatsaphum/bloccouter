import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'couter_event.dart';
part 'couter_state.dart';

class CounterBloc extends Bloc<CounterEvent, int> {
  CounterBloc() : super(0) {
    on<addNumber>((event, emit) => emit(state + 1));
    on<removeNumber>((event, emit) => emit(state - 1));
  }
}
